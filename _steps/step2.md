---
layout: default
title: "Step 2"
permalink: /step2/
---

{% scratchblocks %}
when green flag clicked
forever
  turn cw (15) degrees
  say [Hello!] for (2) seconds
  if <mouse down?> then
    change [mouse clicks v] by (1)
  end
end
{% endscratchblocks %}